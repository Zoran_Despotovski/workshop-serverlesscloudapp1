using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace ServerlessCloudApp1
{
    public static class Function1
    {
        [FunctionName("Function1")]

        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log,
            Binder binder)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            //Get request data
            string name = req.Query["name"];
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;


            //save blob
            log.LogInformation($"CreateBlobUsingBinder function processing: {name}");
            try
            {
                var path = $"{Environment.GetEnvironmentVariable("BlobStorage_Container")}/{name}";
                var attributes = new Attribute[]
                {
                    new BlobAttribute(path, FileAccess.Write),
                    new StorageAccountAttribute("BlobStorage_ConnectionString")
                };
                using (var writer = await binder.BindAsync<TextWriter>(attributes))
                {
                    writer.Write($"{name} says: Hello World!");
                }
            }
            catch (Exception ex)
            {
                log.LogError("Blob saving failed with exception", ex);
            }


            return name != null
                ? (ActionResult)new OkObjectResult($"Hello, {name}")
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }
    }
}
