using System;
using System.IO;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using SendGrid.Helpers.Mail;

namespace ServerlessCloudApp1
{
    public static class Function2
    {
        [FunctionName("Function2")]
        public static void Run(
            [BlobTrigger("%BlobStorage_Container%/{name}", Connection = "BlobStorage_ConnectionString")]Stream myBlob, 
            string name, 
            ILogger log,
            [SendGrid(ApiKey = "SendGrid_Key")] out SendGridMessage message
            )
        {
            //Get BlobTrigger content
            log.LogInformation($"C# Blob trigger function Processed blob\n Name:{name} \n Size: {myBlob.Length} Bytes");
            StreamReader reader = new StreamReader(myBlob);
            string myBlobContent = reader.ReadToEnd();
            log.LogInformation($"Blob content is: '{myBlobContent}'");


            //Send email with SendGrid output-Binding
            log.LogInformation($"Send email via Sendgrid");
            message = new SendGridMessage();
            message.Subject = $"Sendgrid email {DateTime.Now.ToString("yyyyMMdd_hhmmss")}";
            message.AddContent("text/html", $"<h1>{myBlobContent}</h1>");
            message.SetFrom(new EmailAddress(Environment.GetEnvironmentVariable("Sendgrid_DefaultEmailAddress_From")));
            message.AddTo(new EmailAddress(Environment.GetEnvironmentVariable("Sendgrid_DefaultEmailAddress_To")));
        }
    }
}
